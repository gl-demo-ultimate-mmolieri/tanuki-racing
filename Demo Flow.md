## CI Pipeline Walkthrough
1. Talk through the concept of a CI pipeline, including child pipelines.
> - Continuous Integration is a fundamental DevOps practice where developers frequently merge their code changes into a central repository. After each merge, automated builds and tests are run, ensuring quick detection of integration errors. GitLab CI/CD pipelines automate these steps, and child pipelines allow for complex workflows by triggering separate pipeline configurations
> - Pipelines are the top-level component of continuous integration, delivery, and deployment
> - Can run automatically when code is merged, on schedule or manually

> - Child Pieplines run under the same project, ref, and commit SHA as the parent pipeline.
> - In the realm of software development, especially in complex processes like continuous integration and deployment (CI/CD), child pipelines play a crucial role. They are essentially sub-processes or segments within a larger pipeline. By breaking down a monolithic task into smaller, more manageable parts, child pipelines enhance readability, allow for more efficient execution, and improve the overall structure of the workflow.

2. Walk the customer through the gitlab-ci.yml file and explain the various components. 
> Pipelines comprise:
> - Jobs, which define what to do. For example, jobs that compile or test code.
> - Stages, which define when to run the jobs. For example, stages that run tests after stages that compile the code.

3. Complete your walkthrough by modifying the pipeline to display a message after a test completes.
 - put at bottom of test job 
 ```plaintext
   after_script:
      - echo "Test is Complete!"
   ```

# `Run pipeline`
## Execution Order: 
1. Discuss job execution order.
> - Jobs within the same stage run in parallel by default, but stages run sequentially
> - Jobs are picked up by runners as they come available  

2. Demonstrate how to run two (2) jobs in parallel.
> add this new job at bottom and also adds ``needs: []`` to other test job
  ```plaintext
   super_fast_test:
     stage: test
     script:
       - echo "If youre not first youre last"
       - return 0
     needs: []
   ```

## Caching:
1. Explain the purpose of caching and when someone would want to implement it.
> - Caching is used to store job dependencies, allowing subsequent runs to be faster, as the dependencies do not need to be downloaded or generated again.
> - Effective caching can significantly reduce build times and improve overall pipeline performance, especially in projects with heavy dependencies.

2. Demonstrate how to implement caching in your CI pipeline.
> - We saw after committing our last change that the pipeline does take some time to run. Our next few pipelines will build off of our current configuration, so why dont we add some caching for the _vendor/ruby & Gemfile.lock_ so that they dont have to be pulled next time.
> -  Lets go ahead add the code below right under our **_image_** definition so that our node modules will be cached:

```plaintext
# Cache modules in between jobs
cache:
  - key: cache-$CI_COMMIT_REF_SLUG
    fallback_keys:
      - cache-$CI_DEFAULT_BRANCH
      - cache-default
    paths:
      - vendor/ruby
      - Gemfile.lock
```
> -  Now that this code is added the shared runners will cache this information in GCP Cloud Storage for us to pull next time we run the pipeline. Before committing we will complete the next step.
# `Run pipeline`
## Rules:
1. Discuss the purpose of Rules in a CI pipeline.
> -  'rules' are conditions set on jobs within the CI/CD pipeline configuration
> - You can determine when a job should run based on specific criteria like branch names, file changes, or manual triggers. 
> - This feature enhances the efficiency of pipelines by ensuring that jobs are executed only when needed, thus saving resources and time.

2. Demonstrate how to implement rules in your CI pipeline.
> - add to bottom of `super fast test job`. This only runs the job when merged into main
  ```plaintext
   rules:
     - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
   ```

# `Run pipeline on new branch`
## Allowing Failure:
1. Explain through how to allow a job to fail and when this parameter may be used.
> - Sometimes, a job's failure is not critical to your pipeline's success. GitLab allows you to specify jobs that can fail without affecting the entire pipeline.
> - This feature is invaluable for maintaining a robust CI/CD process while exploring new features or making high-risk changes.

2. Demonstrate how to implement allowing a job to fail in your CI pipeline.
> - replace ``super_fast_test`` with this block. changes script to force fail then uses rules to allow failure
```plaintext
super_fast_test:
 stage: test
 script:
 - exit 1
 needs: []
 rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      allow_failure: true
```
# `Run pipeline and show no test complete`
## SAST:
1. Explain SAST and how to include security templates into a pipeline.
> - Static Application Security Testing (SAST) is crucial for identifying vulnerabilities early in the development process. GitLab provides SAST templates for easy integration.
> - Static Application Security Testing (SAST) uses analyzers to detect vulnerabilities in source code
> - Incorporating SAST into your pipeline is a proactive step towards ensuring code security and quality from the early stages of development.

2. Demonstrate adding a SAST template to your pipeline.
> - Under where we define the image for our pipeline we will add the below code to include the SAST template
  ```plaintext
   include:
    - template: Security/SAST.gitlab-ci.yml
  ```

> - Create a new job to run sast in parallel from the start.
> - also add new stage

 ```plaintext
   sast:
     stage: security
     needs: []
   ```

3. Explain why a customer would want to overwrite some of the functionality of the SAST template.

> - You might want to override some SAST settings to tailor security checks to your specific project needs or languages. 

## Build Artifacts:
1. Explain what kinds of build artifacts can be stored in GitLab.
> -  Build artifacts are files generated during jobs that can be stored and accessed after the pipeline completes. This could include compiled code of various languages, test reports, or binaries
> - Storing and accessing build artifacts is essential for continuous delivery and deployment, providing a streamlined process for handling the outputs of your CI/CD pipeline
> - 

2. Demonstrate how to implement a Build stage and how to access the Build artifacts after the pipeline completes.
> - replace whole build job with this  
```plaintext
build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  before_script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $IMAGE .
  after_script:
    - docker push $IMAGE
  artifacts:
    paths:
      - Gemfile.lock
    expire_in: 1 hour
```
# `Run pipeline and show SAST complete and show artifacts download area`
